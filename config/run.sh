#!/usr/bin/env bash

cd ../under-boot-server/ && echo "Packaging Backend ..."

mvn package

cd ../config/
docker-compose up