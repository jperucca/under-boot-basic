import moment from 'moment'
import axios from 'axios'
import config from '../../config'

axios.defaults.baseURL = config.url;

export default {
  axios (url, options) {
    return new AxiosWrapper(url, options);
  },

  addRequestInterceptor (requestSent, requestError) {
    return axios.interceptors.request.use(requestSent, requestError);
  },

  removeRequestInterceptor (requestSent, requestError) {
    axios.interceptors.request.eject(requestSent, requestError);
    return this;
  },

  addResponseInterceptor (responseSent, responseError) {
    return axios.interceptors.response.use(responseSent, responseError);
  },

  removeResponseInterceptor (responseSent, responseError) {
    axios.interceptors.response.eject(responseSent, responseError);
    return this;
  },

  formatDate (date, {start_of = 'day', utc = false, format = 'DD/MM/YYYY'} = {}) {
    var moment_date = moment(date, format);

    if (utc) {
      moment_date = moment_date.utc();
    }

    if (start_of) {
      moment_date = moment_date.startOf(start_of);
    }

    return moment_date.format('YYYY-MM-DDTHH:mm:ss.SSS');
  }
}

class AxiosWrapper {
  constructor (url, options = {}) {
    this.url = url;
    this.options = this._cleanOptions(options);
  }

  create (url, options = {}) {
    url = this.url + url;
    options = this._mergeToOptions(options);

    return new AxiosWrapper(url, options);
  }

  request (options) {
    return axios.request(options);
  }

  get (options) {
    options = this._mergeToOptions(options);
    return axios.get(this._addUrlFromOptions(this.url, options), options);
  }

  delete (options) {
    options = this._mergeToOptions(options);
    return axios.delete(this._addUrlFromOptions(this.url, options), options);
  }

  head (options) {
    options = this._mergeToOptions(options);
    return axios.head(this._addUrlFromOptions(this.url, options), options);
  }

  post (data, options) {
    options = this._mergeToOptions(options);
    return axios.post(this._addUrlFromOptions(this.url, options), data, options);
  }

  put (data, options) {
    options = this._mergeToOptions(options);
    return axios.put(this._addUrlFromOptions(this.url, options), data, options);
  }

  patch (data, options) {
    options = this._mergeToOptions(options);
    return axios.patch(this._addUrlFromOptions(this.url, options), data, options);
  }

  _cleanOptions (options = {}) {
    if (typeof options === 'string' || typeof options === 'number') {
      options = {
        url: options
      }
    }

    return options;
  }

  _addUrlFromOptions (url, options = {}) {
    url += options.url ? options.url.toString().replace(/^([^/]{1})/, '/$1') : '';
    return url;
  }

  _mergeToOptions (options = {}) {
    options = this._cleanOptions(options);
    return Object.assign({}, this.options, options);
  }
}