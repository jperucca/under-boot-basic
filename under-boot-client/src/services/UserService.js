import Api from './api/Api'

const api_user = Api.axios('/users');
const api_user_signup = api_user.create('/signup');
const api_user_me = api_user.create('/me');

var userApi = {
    createUser(signupDTO) {
        return api_user_signup.post(signupDTO);  
    },
    getUser() {
        return api_user_me.get();
    },
    updateUser(userDTO) {
        return api_user_me.put(userDTO);
    }
};

export default Object.assign(userApi, Api);