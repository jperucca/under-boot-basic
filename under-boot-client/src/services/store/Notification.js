import Store from './Store'

export default {
    state: Store.state,

    dismiss (alert) {
        var alertIndex = this.state.alerts.indexOf(alert);

        this.state.alerts.splice(alertIndex, 1)
    },

    add (type, text, timeout = 5000) {
        var alert = { type, text, timeout };

        this.state.alerts.push(alert);
        setTimeout(() => this.dismiss(alert), timeout);
    }
}