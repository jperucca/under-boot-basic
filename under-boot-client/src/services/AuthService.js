import Api from './api/Api'
import UserService from './UserService'

const api_login = Api.axios('/login');

export default {
    store : {
        authenticated: false,
        currentUser: {
            username: "User"
        }
    },

    storageKey: 'jwtToken',
    _token: null,
    _router: {},

    init (router) {
        Api.addRequestInterceptor(this._requestInterceptor.bind(this));
        Api.addResponseInterceptor(null, this._responseErrorInterceptor.bind(this));
        this._router = router;
        this.store.authenticated = this._getToken() !== null;
        if(this.store.currentUser.username === "User" && localStorage.getItem(this.storageKey) != null)
            this._loadUser();
    },
    isLoggedIn () {
        return this.store.authenticated;
    },
    login (creditals) {
        return api_login.post(creditals)
            .then(this._loginSuccess.bind(this));
    },
    logout () {
        localStorage.removeItem(this.storageKey);
        this.store.authenticated = false;
    },
    getUser () {
        return this.store.currentUser;    
    },
    _loadUser() {
        UserService.getUser()
            .then(this._setCurrentUser.bind(this));
    },
    _loginSuccess (response) {
        console.log('[LOGIN] Success');
        
        this._setToken(response.headers.authorization);

        this._loadUser();
    },
    _getToken () {
        var token = this._token || localStorage.getItem(this.storageKey);
        return token || null;
    },
    _setToken (token) {
        this._token = token;
        localStorage.setItem(this.storageKey, token);
        this.store.authenticated = true;
    },
    _setCurrentUser (response) {
        this.store.currentUser = response.data;
    },
    _requestInterceptor (request) {
        var token = this._getToken();
        if (token) {
            request.headers.Authorization = token;
        }

        return request;
    },
    _responseErrorInterceptor (error) {
        if (error.response.status === 401) {
            this.isLoggedIn() && AlertActions.add('danger', 'Vous avez été déconnecté');
            this.logout();
            this._router.go({name: 'login'});
        }

        return Promise.reject(error)
    }
}