import Vue from 'vue'
import VueRouter from 'vue-router'
import Landing from '@/components/Landing'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
import Alert from '@/components/Alert'
import UserMe from '@/components/UserMe'
import AuthService from '@/services/AuthService'

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    },
    {
      path: '/user',
      name: 'UserMe',
      component: UserMe
    },
    {
      path: '/alerts',
      name: 'Alerts',
      component: Alert,
      meta: {
        requiresAuth: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && !AuthService.isLoggedIn()) {
      next({ name: 'Login' });
  }

  next();
});

export default router;