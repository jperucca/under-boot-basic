INSERT INTO secured_user (id, email, username, password, enabled, reset_date) VALUES (1, 'admin@under.com', 'admin', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi', 1, now());
INSERT INTO secured_user (id, email, username, password, enabled, reset_date) VALUES (2, 'under@under.com', 'user', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 1, now());
INSERT INTO secured_user (id, email, username, password, enabled, reset_date) VALUES (3, 'disabled@under.com','disabled', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 0, now());
INSERT INTO authority (id, role) VALUES (1, 'ROLE_USER');
INSERT INTO authority (id, role) VALUES (2, 'ROLE_ADMIN');
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (1, 1);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (1, 2);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (2, 1);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (3, 1);