CREATE TABLE secured_user (
  id INT(11) PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(80),
  username VARCHAR(80),
  password VARCHAR(255),
  enabled INT(1),
  lastpasswordresetdate DATE
);

CREATE TABLE authority (
  id INT(2) PRIMARY KEY AUTO_INCREMENT,
  role VARCHAR(50)
);

CREATE TABLE USER_AUTHORITY (
  user_id INT(11) PRIMARY KEY AUTO_INCREMENT,
  authority_id INT(11),
  FOREIGN KEY (user_id) REFERENCES secured_user(id),
  FOREIGN KEY (authority_id) REFERENCES authority(id)
);