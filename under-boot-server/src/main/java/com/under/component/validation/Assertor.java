package com.under.component.validation;

import org.springframework.util.Assert;

public final class Assertor {

    private Assertor() {}

    public static <T> void notNull(T object) {
        Assert.notNull(object, "Cannot process empty 'request' parameter");
    }
}
