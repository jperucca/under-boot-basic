package com.under.component.statemachine.service;

import com.under.component.statemachine.domain.StateMachineDefinition;
import com.under.component.statemachine.domain.StateMachineModel;
import com.under.component.statemachine.exception.StateMachineCreationException;
import com.under.component.statemachine.internal.StateMachineFactoryAdapter;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.TreeSet;

@Service
public class StateMachineConverter {

    public StateMachineDefinition convert(StateMachineModel model, BeanFactory beanFactory) throws StateMachineCreationException {
        String initialState = model.getInitialState();
        String terminalState = model.getTerminalState();
        TreeSet<String> states = model.getStates();
        Map<String, Pair<String, String>> transitions = model.getTransitionsByEvent();

        StateMachine<String, String> stateMachine = StateMachineFactoryAdapter.builder()
                .states(states)
                .transitions(transitions)
                .initialState(initialState)
                .terminalState(terminalState)
                .configuration(beanFactory)
                .build();

        return StateMachineDefinition.builder()
                .stateMachine(stateMachine)
                .build();
    }
}
