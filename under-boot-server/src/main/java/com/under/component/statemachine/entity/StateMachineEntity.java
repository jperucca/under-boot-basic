package com.under.component.statemachine.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class StateMachineEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String clientUuid;

    @Lob
    @Column
    private StateMachineModelEntity stateMachineModelEntity;

}
