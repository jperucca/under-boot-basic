package com.under.component.statemachine.service;

import com.under.component.client.domain.ClientContract;
import com.under.component.statemachine.domain.StateMachineDefinition;
import com.under.component.statemachine.domain.StateMachineModel;
import com.under.component.statemachine.entity.StateMachineEntity;
import com.under.component.statemachine.entity.StateMachineModelEntity;
import com.under.component.statemachine.entity.StateMachineRepository;
import com.under.component.statemachine.exception.StateMachineCreationException;
import com.under.framework.TransactionalService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

@TransactionalService
public class CreateStateMachineService {

    private final StateMachineRepository stateMachineRepository;
    private final StateMachineConverter stateMachineConverter;

    @Autowired
    public CreateStateMachineService(StateMachineRepository stateMachineRepository,
                                     StateMachineConverter stateMachineConverter) {
        this.stateMachineRepository = stateMachineRepository;
        this.stateMachineConverter = stateMachineConverter;
    }

    public StateMachineDefinition createStateMachine(ClientContract clientContract,
                                                     StateMachineModel model,
                                                     BeanFactory beanFactory) throws StateMachineCreationException {
        StateMachineDefinition definition = stateMachineConverter.convert(model, beanFactory);

        StateMachineEntity stateMachineEntity = new StateMachineEntity();
        stateMachineEntity.setClientUuid( clientContract.getClientId().getUuid() );
        stateMachineEntity.setStateMachineModelEntity( StateMachineModelEntity.from(model) );

        stateMachineRepository.save(stateMachineEntity);

        return definition;
    }
}
