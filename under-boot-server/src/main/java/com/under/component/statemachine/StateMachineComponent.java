package com.under.component.statemachine;

import com.under.component.client.domain.ClientContract;
import com.under.component.statemachine.domain.StateMachineDefinition;
import com.under.component.statemachine.domain.StateMachineModel;
import com.under.component.statemachine.exception.StateMachineCreationException;
import com.under.component.statemachine.service.CreateStateMachineService;
import com.under.component.statemachine.service.StateMachineConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class StateMachineComponent {

    private final CreateStateMachineService createStateMachineService;
    private final StateMachineConverter stateMachineConverter;
    private final ApplicationContext context;

    @Autowired
    public StateMachineComponent(CreateStateMachineService createStateMachineService,
                                 StateMachineConverter stateMachineConverter,
                                 ApplicationContext context) {
        this.createStateMachineService = createStateMachineService;
        this.stateMachineConverter = stateMachineConverter;
        this.context = context;
    }

    public StateMachineDefinition createStateMachine(ClientContract clientContract, StateMachineModel model) throws StateMachineCreationException {
        return createStateMachineService.createStateMachine(clientContract, model, getBeanFactory());
    }

    public StateMachineDefinition convert(StateMachineModel model) throws StateMachineCreationException {
        return stateMachineConverter.convert(model, getBeanFactory());
    }

    private AutowireCapableBeanFactory getBeanFactory() {
        return context.getAutowireCapableBeanFactory();
    }
}

