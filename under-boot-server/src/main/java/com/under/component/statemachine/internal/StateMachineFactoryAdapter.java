package com.under.component.statemachine.internal;

import com.under.component.statemachine.domain.StateMachineModel;
import com.under.component.statemachine.domain.StateMachineModel.StateMachineModelBuilder;
import com.under.component.statemachine.exception.StateMachineCreationException;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.Map;
import java.util.TreeSet;

public final class StateMachineFactoryAdapter {
    
    public static ClientStateMachineBuilder builder() {
        return new ClientStateMachineBuilder( StateMachineBuilder.builder() );
    }

    public static class ClientStateMachineBuilder {

        private final StateMachineBuilder.Builder<String, String> builder;
        private StateMachineModelBuilder modelBuilder;
        private BeanFactory beanFactory;

        public ClientStateMachineBuilder(StateMachineBuilder.Builder<String, String> builder) {
            this.builder = builder;
            this.modelBuilder = StateMachineModel.builder();
        }

        public ClientStateMachineBuilder initialState(String initialState) {
            this.modelBuilder.initialState(initialState);
            return this;
        }

        public ClientStateMachineBuilder terminalState(String terminalState) {
            this.modelBuilder.terminalState(terminalState);
            return this;
        }

        public ClientStateMachineBuilder states(TreeSet<String> states) {
            this.modelBuilder.states(states);
            return this;
        }

        public ClientStateMachineBuilder transitions(Map<String, Pair<String, String>> transitions) {
            this.modelBuilder.transitionsByEvent(transitions);
            return this;
        }

        public ClientStateMachineBuilder configuration(BeanFactory beanFactory) {
            this.beanFactory = beanFactory;
            return this;
        }

        public StateMachine<String, String> build() throws StateMachineCreationException {
            StateMachineModel stateMachineModel = modelBuilder.build();

            if (builder == null || !stateMachineModel.isValid() || beanFactory == null) {
                throw new StateMachineCreationException("StateMachine could not be created");
            }
            try {
                configureStates(stateMachineModel, builder);
                configureTransitions(stateMachineModel, builder);
                configureMachine(beanFactory, builder);
                return builder.build();

            } catch (Exception e) {
                throw new StateMachineCreationException("StateMachine could not be created", e);
            }
        }

        private void configureStates(StateMachineModel stateMachineModel,
                                     StateMachineBuilder.Builder<String, String> builder) throws Exception {
            builder.configureStates().withStates()
                    .initial(stateMachineModel.getInitialState())
                    .end(stateMachineModel.getTerminalState())
                    .states(stateMachineModel.getStates());
        }

        private void configureTransitions(StateMachineModel stateMachineModel,
                                          StateMachineBuilder.Builder<String, String> builder) throws Exception {
            StateMachineTransitionConfigurer<String, String> transitionConfigurer = builder.configureTransitions();
            Map<String, Pair<String, String>> transitionsByEvent = stateMachineModel.getTransitionsByEvent();

            for (Map.Entry<String, Pair<String, String>> entry : transitionsByEvent.entrySet()) {
                String event = entry.getKey();
                String from = entry.getValue().getKey();
                String to = entry.getValue().getValue();

                transitionConfigurer.withExternal().source(from).target(to).event(event).and().withExternal();
            }
        }

        private void configureMachine(BeanFactory beanFactory,
                                      StateMachineBuilder.Builder<String, String> builder) throws Exception {
            builder.configureConfiguration().withConfiguration()
                    .beanFactory(beanFactory)
                    .autoStartup(false);
        }
    }
}