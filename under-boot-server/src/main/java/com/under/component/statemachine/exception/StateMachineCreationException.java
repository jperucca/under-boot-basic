package com.under.component.statemachine.exception;

import com.under.framework.BusinessException;

public class StateMachineCreationException extends BusinessException {

    public StateMachineCreationException(String message, Exception exception) {
        super(message, exception);
    }

    public StateMachineCreationException(String message) {
        super(message);
    }
}
