package com.under.component.statemachine.domain;

import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.TreeSet;

@Builder
@Getter
public class StateMachineModel {
    private String initialState;
    private String terminalState;

    private TreeSet<String> states;
    // key = event | value = transition between 2 states (first to second)
    private Map<String, Pair<String, String>> transitionsByEvent;

    public boolean isValid() {
        return initialState != null &&
                terminalState != null &&
                states != null &&
                !states.isEmpty() &&
                transitionsByEvent != null &&
                !transitionsByEvent.isEmpty();
    }
}
