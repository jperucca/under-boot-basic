package com.under.component.statemachine.entity;

import com.under.component.statemachine.domain.StateMachineModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import javax.persistence.Embeddable;
import javax.persistence.Lob;
import java.util.HashMap;
import java.util.TreeSet;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Embeddable
public class StateMachineModelEntity {

    private String initialState;
    private String terminalState;
    private TreeSet<String> states;
    @Lob
    private HashMap<String, Pair<String, String>> transitionsByEvent;

    public static StateMachineModelEntity from(StateMachineModel model) {
        StateMachineModelEntity entity = new StateMachineModelEntity();

        entity.setInitialState( model.getInitialState() );
        entity.setTerminalState( model.getTerminalState() );
        entity.setStates( model.getStates() );
        entity.setTransitionsByEvent( new HashMap<>(model.getTransitionsByEvent()) );

        return entity;
    }
}
