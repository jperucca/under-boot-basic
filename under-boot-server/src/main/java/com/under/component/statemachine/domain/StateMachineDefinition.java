package com.under.component.statemachine.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.statemachine.StateMachine;

@Builder
@Data
public class StateMachineDefinition {

    private StateMachine<String, String> stateMachine;

    public String getInitialState() {
        return stateMachine.getInitialState().getId();
    }
}
