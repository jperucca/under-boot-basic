package com.under.component.exchange;

import com.under.component.exchange.domain.Exchange;
import com.under.component.exchange.request.CreateExchangeRequest;
import com.under.component.exchange.service.CreateExchangeService;
import com.under.framework.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExchangeComponent {

    private final CreateExchangeService createExchangeService;

    @Autowired
    public ExchangeComponent(CreateExchangeService createExchangeService) {
        this.createExchangeService = createExchangeService;
    }

    public Exchange createExchange(CreateExchangeRequest request) throws BusinessException {
        return createExchangeService.createExchange(request);
    }

}
