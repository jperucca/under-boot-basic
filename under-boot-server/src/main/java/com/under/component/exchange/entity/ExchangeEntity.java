package com.under.component.exchange.entity;

import com.under.component.client.domain.ClientId;
import com.under.component.exchange.domain.Exchange;
import com.under.component.exchange.domain.TimeCategoryType;
import com.under.component.exchange.domain.Sharing;
import com.under.component.exchange.domain.SharingType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class ExchangeEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String clientUuid;

    @Column(nullable = false)
    private String state;

    @Column(nullable = false)
    @Enumerated(STRING)
    private SharingType type;

    @Column(nullable = false)
    @Enumerated(STRING)
    private TimeCategoryType timeCategoryType;

    @Column
    private String owner;

    @Column
    private String receiver;

    public static ExchangeEntity of(Exchange exchange, ClientId clientId) {
        Sharing sharing = exchange.getSharing();
        ExchangeEntity exchangeEntity = new ExchangeEntity();

        exchangeEntity.setClientUuid(clientId.getUuid());

        exchangeEntity.setState(exchange.getState());
        exchangeEntity.setType(sharing.getSharingType());
        exchangeEntity.setOwner(sharing.getOwner());
        exchangeEntity.setReceiver(sharing.getReceiver());
        exchangeEntity.setTimeCategoryType(exchange.getTimeCategory().getType());

        return exchangeEntity;
    }
}
