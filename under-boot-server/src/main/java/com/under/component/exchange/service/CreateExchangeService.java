package com.under.component.exchange.service;

import com.under.component.client.ContractComponent;
import com.under.component.exchange.domain.Exchange;
import com.under.component.exchange.domain.TimeCategory;
import com.under.component.exchange.domain.Sharing;
import com.under.component.exchange.request.CreateExchangeRequest;
import com.under.component.statemachine.domain.StateMachineDefinition;
import com.under.component.validation.Assertor;
import com.under.framework.BusinessException;
import com.under.framework.TransactionalService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import static org.slf4j.LoggerFactory.getLogger;

@TransactionalService
public class CreateExchangeService {

    private final Logger logger = getLogger(CreateExchangeService.class);

    private final ContractComponent contractComponent;
    private final ExchangeService exchangeService;

    @Autowired
    public CreateExchangeService(ContractComponent contractComponent, ExchangeService exchangeService) {
        this.contractComponent = contractComponent;
        this.exchangeService = exchangeService;
    }

    public Exchange createExchange(CreateExchangeRequest request) throws BusinessException {
        Assertor.notNull(request);

        request.validate();
        contractComponent.checkValidity(request);
        Exchange exchangeCreated = this.create(request);

        logger.trace("Exchange Creation [[ SUCCESS ]]");

        return exchangeCreated;
    }

    private Exchange create(CreateExchangeRequest request) {
        StateMachineDefinition stateMachineDefintion = request.getStateMachineDefintion();
        String initialState = stateMachineDefintion.getInitialState();

        Exchange exchange = from(request);
        exchange.setState(initialState);

        exchangeService.save(exchange, request.getClientId());

        return exchange;
    }

    private Exchange from(CreateExchangeRequest request) {
        TimeCategory timeCategory = TimeCategory.builder().type(request.getTimeCategoryType()).build();

        Sharing sharing = Sharing.builder()
                .sharingType(request.getSharingType())
                .items(request.getItems())
                .owner(request.getOwner())
                .receiver(request.getReceiver())
                .build();

        return Exchange.builder()
                .sharing(sharing)
                .timeCategory(timeCategory)
                .build();
    }
}
