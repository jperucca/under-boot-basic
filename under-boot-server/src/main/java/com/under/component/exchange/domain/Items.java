package com.under.component.exchange.domain;

import java.util.ArrayList;
import java.util.List;

public class Items {
    private List<Item> items;

    public Items(List<Item> items) {
        this.items = items;
    }

    public List<Item> getValues() {
        return new ArrayList<>(items);
    }

    public boolean isEmpty() {
        return items != null && items.isEmpty();
    }
}





