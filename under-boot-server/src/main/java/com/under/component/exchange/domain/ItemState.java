package com.under.component.exchange.domain;

import lombok.Data;

@Data
public class ItemState {
    private String state;

    private ItemState(String state) {
        this.state = state;
    }

    public static ItemState from(String state) {
        return new ItemState(state);
    }
}