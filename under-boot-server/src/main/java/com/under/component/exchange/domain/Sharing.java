package com.under.component.exchange.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Sharing {
    private Items items;

    private SharingType sharingType;
    private String owner;
    private String receiver;
}
