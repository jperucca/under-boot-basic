package com.under.component.exchange.domain;

import lombok.Data;

@Data
public class Category {
    private String uuid;
    private String name;

    private Category(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public static Category from(String categoryUuid, String name) {
        return new Category(categoryUuid, name);
    }

    public static Category from(String categoryUuid) {
        return from(categoryUuid, "cat-unknown-id");
    }
}