package com.under.component.exchange.domain;

public enum SharingType {
    SELLING,
    GIVING,
    LOANING
}
