package com.under.component.exchange.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Item {
    private String uuid;
    private String name;
    private ItemState state;
    
    private Category category;
    private String ownerUuid;
}