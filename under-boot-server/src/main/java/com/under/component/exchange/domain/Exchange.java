package com.under.component.exchange.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Exchange {

    private String state;

    private final Sharing sharing;
    private final TimeCategory timeCategory;
}