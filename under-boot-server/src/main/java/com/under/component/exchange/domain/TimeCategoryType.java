package com.under.component.exchange.domain;

public enum TimeCategoryType {
    ONLY_ONCE,
    TIME_RANGE
}