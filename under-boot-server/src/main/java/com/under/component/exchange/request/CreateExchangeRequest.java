package com.under.component.exchange.request;

import com.under.component.client.domain.ClientContract;
import com.under.component.client.request.ContractedRequest;
import com.under.component.client.domain.SharingSystem;
import com.under.component.exchange.domain.TimeCategoryType;
import com.under.component.exchange.domain.Items;
import com.under.component.validation.Assertor;
import lombok.Builder;
import lombok.Data;
import org.springframework.util.Assert;

@Data
public class CreateExchangeRequest extends ContractedRequest {
    private TimeCategoryType timeCategoryType;
    private Items items;
    private String owner;
    private String receiver;

    @Builder
    public CreateExchangeRequest(ClientContract contract, SharingSystem sharingSystem, TimeCategoryType timeCategoryType, Items items, String owner, String receiver) {
        super(contract, sharingSystem);
        this.timeCategoryType = timeCategoryType;
        this.items = items;
        this.owner = owner;
        this.receiver = receiver;
    }

    public void validate() {
        Assertor.notNull(items);
        Assertor.notNull(timeCategoryType);

        Assert.isTrue(!items.isEmpty(), "Sharing items cannot be empty");
        Assert.hasText(owner, "owner must be defined");
        Assert.hasText(receiver, "receiver must be defined");
    }
}
