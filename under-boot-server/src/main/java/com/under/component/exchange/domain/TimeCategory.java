package com.under.component.exchange.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TimeCategory {
    private TimeCategoryType type;

    public TimeCategory(TimeCategoryType type) {
        this.type = type;
    }

}