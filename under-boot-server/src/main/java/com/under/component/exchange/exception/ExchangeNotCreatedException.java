package com.under.component.exchange.exception;

public class ExchangeNotCreatedException extends RuntimeException {

    public ExchangeNotCreatedException(String message, Throwable cause) {
        super(message, cause);
    }
}
