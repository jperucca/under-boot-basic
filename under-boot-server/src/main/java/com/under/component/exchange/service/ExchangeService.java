package com.under.component.exchange.service;

import com.under.component.client.domain.ClientId;
import com.under.component.exchange.domain.Exchange;
import com.under.component.exchange.entity.ExchangeRepository;
import com.under.framework.TransactionalService;
import com.under.component.exchange.entity.ExchangeEntity;
import com.under.component.exchange.exception.ExchangeNotCreatedException;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

@TransactionalService
public class ExchangeService {

    private final Logger logger = getLogger(ExchangeService.class);

    private final ExchangeRepository exchangeRepository;

    public ExchangeService(ExchangeRepository exchangeRepository) {
        this.exchangeRepository = exchangeRepository;
    }

    public void save(Exchange exchange, ClientId clientId) {
        try {
            ExchangeEntity exchangeEntity = ExchangeEntity.of(exchange, clientId);
            exchangeRepository.save(exchangeEntity);
        } catch (RuntimeException re) {

            String message = "Could not save ExchangeEntity";
            logger.error(message, re);
            throw new ExchangeNotCreatedException(message, re);
        }
    }
}
