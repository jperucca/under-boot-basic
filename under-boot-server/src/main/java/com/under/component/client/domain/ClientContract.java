package com.under.component.client.domain;

import com.under.component.exchange.domain.SharingType;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ClientContract {

    private ClientId clientId;
    private SharingType sharingType;
}
