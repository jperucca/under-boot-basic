package com.under.component.client.domain;

import lombok.Getter;

import java.util.UUID;

@Getter
public class ClientId {
    private final String uuid;

    private ClientId() {
        this.uuid = UUID.randomUUID().toString();
    }

    private ClientId(String uuid) {
        this.uuid = uuid;
    }

    public static ClientId from(String value) {
        return new ClientId(value);
    }
    public static ClientId generate() {
        return new ClientId();
    }
}
