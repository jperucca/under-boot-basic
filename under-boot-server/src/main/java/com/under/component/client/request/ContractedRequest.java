package com.under.component.client.request;

import com.under.component.client.domain.SharingSystem;
import com.under.component.client.domain.ClientContract;
import com.under.component.client.domain.ClientId;
import com.under.component.exchange.domain.SharingType;
import com.under.component.statemachine.domain.StateMachineDefinition;

public class ContractedRequest {

    private ClientContract contract;
    private SharingSystem sharingSystem;

    public ContractedRequest(ClientContract contract, SharingSystem sharingSystem) {
        this.contract = contract;
        this.sharingSystem = sharingSystem;
    }

    public ClientId getClientId() {
        return contract.getClientId();
    }

    public SharingType getSharingType() {
        return sharingSystem.getSharingType();
    }

    public StateMachineDefinition getStateMachineDefintion() {
        return sharingSystem.getDefinition();
    }
}
