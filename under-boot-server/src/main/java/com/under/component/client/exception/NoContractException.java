package com.under.component.client.exception;

import com.under.framework.BusinessException;

import static java.lang.String.format;

public class NoContractException extends BusinessException {

    public NoContractException(String clientUuid) {
        super(format("%s has no contract", clientUuid));
    }
}
