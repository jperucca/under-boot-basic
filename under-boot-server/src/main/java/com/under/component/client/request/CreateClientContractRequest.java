package com.under.component.client.request;

import com.under.component.client.domain.ClientContract;
import com.under.component.exchange.domain.SharingType;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CreateClientContractRequest {

    private ClientContract contract;
    private SharingType sharingType;
}
