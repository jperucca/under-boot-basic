package com.under.component.client.service;

import com.under.component.client.domain.ClientContract;
import com.under.component.client.entity.ContractEntity;
import com.under.component.client.entity.ContractRepository;
import com.under.component.client.entity.SharingTypes;
import com.under.component.client.request.CreateClientContractRequest;
import com.under.component.exchange.domain.SharingType;
import com.under.framework.TransactionalService;
import org.springframework.beans.factory.annotation.Autowired;

@TransactionalService
public class CreateClientContractService {

    private final ContractRepository repository;

    @Autowired
    public CreateClientContractService(ContractRepository repository) {
        this.repository = repository;
    }

    public ClientContract createClientContract(CreateClientContractRequest request) {
        ClientContract contract = request.getContract();
        SharingType sharingType = request.getSharingType();

        ContractEntity contractEntity = new ContractEntity();
        contractEntity.setClientUuid( contract.getClientId().getUuid() );
        contractEntity.setSharingTypes( SharingTypes.from(sharingType) );

        repository.save(contractEntity);

        return contract;
    }
}
