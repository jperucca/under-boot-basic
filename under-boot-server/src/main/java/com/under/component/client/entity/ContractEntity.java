package com.under.component.client.entity;

import com.under.component.exchange.domain.SharingType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter

@Entity
public class ContractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String clientUuid;

    @Embedded
    private SharingTypes sharingTypes;

    public boolean isEligible(SharingType sharingType) {
        return sharingTypes.getValues().contains(sharingType);
    }
}
