package com.under.component.client;

import com.under.component.client.domain.ClientContract;
import com.under.component.client.exception.InEligibleException;
import com.under.component.client.exception.NoContractException;
import com.under.component.client.request.CreateClientContractRequest;
import com.under.component.client.service.CreateClientContractService;
import com.under.component.client.service.ValidityService;
import com.under.component.exchange.request.CreateExchangeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContractComponent {

    private final ValidityService validityService;
    private final CreateClientContractService createClientContractService;

    @Autowired
    public ContractComponent(ValidityService validityService,
                             CreateClientContractService createClientContractService) {
        this.validityService = validityService;
        this.createClientContractService = createClientContractService;
    }

    public ClientContract createClientContract(CreateClientContractRequest request) {
        return createClientContractService.createClientContract(request);
    }

    public void checkValidity(CreateExchangeRequest request) throws InEligibleException, NoContractException {
        validityService.checkValidity(request);
    }
}
