package com.under.component.client.exception;

import com.under.component.client.entity.ContractEntity;
import com.under.component.exchange.domain.SharingType;
import com.under.framework.BusinessException;

public class InEligibleException extends BusinessException {

    public InEligibleException(ContractEntity contractEntity, SharingType sharingType) {
        super(contractEntity.getClientUuid() + " ineligible for " + sharingType +
            " sharingTypesEligibles : [[ " + contractEntity.getSharingTypes().getValues() + " ]]");
    }
}
