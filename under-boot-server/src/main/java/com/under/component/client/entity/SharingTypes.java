package com.under.component.client.entity;

import com.under.component.exchange.domain.SharingType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.HashSet;
import java.util.Set;

@Embeddable
@Getter
@Setter
public class SharingTypes {

    @Transient
    private Set<SharingType> values;

    @Column
    private boolean selling;

    @Column
    private boolean giving;

    @Column
    private boolean loaning;

    SharingTypes() {}

    public static SharingTypes from(SharingType... sharingTypes) {
        SharingTypes result = new SharingTypes();

        for (SharingType sharingType : sharingTypes) {
            switch (sharingType) {
                case SELLING: result.setSelling(true); break;
                case GIVING: result.setGiving(true); break;
                case LOANING: result.setLoaning(true); break;
            }
        }
        return result;
    }

    public Set<SharingType> getValues() {
        if (values == null) loadSet();

        return new HashSet<>(values);
    }

    private void loadSet() {
        this.values = new HashSet<>();

        if (selling) {
            values.add(SharingType.SELLING);
        }
        if (giving) {
            values.add(SharingType.GIVING);
        }
        if (loaning) {
            values.add(SharingType.LOANING);
        }
    }
}
