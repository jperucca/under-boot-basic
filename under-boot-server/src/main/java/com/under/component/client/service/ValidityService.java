package com.under.component.client.service;

import com.under.component.client.entity.ContractEntity;
import com.under.component.client.entity.ContractRepository;
import com.under.component.client.request.ContractedRequest;
import com.under.component.client.exception.InEligibleException;
import com.under.component.client.exception.NoContractException;
import com.under.component.exchange.domain.SharingType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ValidityService {

    private final ContractRepository repository;

    @Autowired
    public ValidityService(ContractRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public void checkValidity(ContractedRequest request) throws NoContractException, InEligibleException {
        String clientUuid = request.getClientId().getUuid();
        SharingType sharingType = request.getSharingType();

        Optional<ContractEntity> optionalContract = repository.findByClientUuid(clientUuid);
        if (!optionalContract.isPresent()) {
            throw new NoContractException(clientUuid);
        }


        ContractEntity contractEntity = optionalContract.get();
        if( !contractEntity.isEligible(sharingType) ) {
            throw new InEligibleException(contractEntity, sharingType);
        }
    }
}
