package com.under.component.client.domain;

import com.under.component.exchange.domain.SharingType;
import com.under.component.statemachine.domain.StateMachineDefinition;
import lombok.Getter;

@Getter
public class SharingSystem {

    private final SharingType sharingType;
    private final StateMachineDefinition definition;

    public SharingSystem(SharingType sharingType, StateMachineDefinition definition) {
        this.sharingType = sharingType;
        this.definition = definition;
    }
}
