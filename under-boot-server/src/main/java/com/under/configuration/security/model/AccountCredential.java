package com.under.configuration.security.model;

import lombok.Data;

@Data
public class AccountCredential {

    private String username;
    private String password;
}