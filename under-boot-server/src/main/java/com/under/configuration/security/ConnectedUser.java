package com.under.configuration.security;

import com.under.configuration.security.jwt.JwtUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class ConnectedUser implements Authentication {

    private final JwtUser userDetails;
    private boolean authenticated = true;

    public ConnectedUser(UserDetails userDetails) {
        this.userDetails = (JwtUser) userDetails;
    }

    public Long getId() {
        return userDetails.getId();
    }

    public String getEmail() {
        return userDetails.getEmail();
    }

    @Override
    public String getName() {
        return userDetails.getUsername();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userDetails.getAuthorities();
    }

    @Override
    public Object getCredentials() {
        return userDetails.getPassword();
    }

    @Override
    public UserDetails getDetails() {
        return userDetails;
    }

    @Override
    public Object getPrincipal() {
        return userDetails.getUsername();
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
