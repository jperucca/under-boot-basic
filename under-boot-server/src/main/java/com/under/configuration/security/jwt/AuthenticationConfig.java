package com.under.configuration.security.jwt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "auth")
@Getter
@Setter
public class AuthenticationConfig {

    private String secret;

    private long expirationTime = 864_000_000; // 10 days
}
