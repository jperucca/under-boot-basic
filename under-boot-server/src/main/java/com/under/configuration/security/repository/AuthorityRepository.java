package com.under.configuration.security.repository;

import com.under.configuration.security.model.Authority;
import com.under.configuration.security.model.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    List<Authority> findByRole(AuthorityName authorityName);
}
