package com.under.configuration.security.jwt;

import com.under.configuration.security.model.SecuredUser;
import com.under.configuration.security.repository.SecuredUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final SecuredUserRepository securedUserRepository;
    private final JwtUserFactory jwtUserFactory;

    @Autowired
    public JwtUserDetailsServiceImpl(SecuredUserRepository securedUserRepository, JwtUserFactory jwtUserFactory) {
        this.securedUserRepository = securedUserRepository;
        this.jwtUserFactory = jwtUserFactory;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final SecuredUser securedUser = securedUserRepository.findByUsername(username);

        if (securedUser == null) {
            System.out.println("UserDetail not found for username : " + username);
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        }

        return jwtUserFactory.create(securedUser);
    }
}
