package com.under.configuration.security.model;

public enum  AuthorityName {
    ROLE_USER,
    ROLE_ADMIN
}
