package com.under.configuration.security.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

import static javax.persistence.EnumType.STRING;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "authority")
public class Authority {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "role", length = 50)
    @NotNull
    @Enumerated(STRING)
    private AuthorityName role;

    @ManyToMany(mappedBy = "authorities", fetch = FetchType.LAZY)
    private List<SecuredUser> users;
}
