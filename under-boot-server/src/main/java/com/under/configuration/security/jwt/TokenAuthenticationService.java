package com.under.configuration.security.jwt;

import com.under.configuration.security.ConnectedUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static io.jsonwebtoken.SignatureAlgorithm.HS512;

@Service
public class TokenAuthenticationService {

    private final AuthenticationConfig authenticationConfig;
    private final UserDetailsService userDetailsService;
    private final String TOKEN_PREFIX = "Bearer";
    private final String HEADER_STRING = "Authorization";

    @Autowired
    TokenAuthenticationService(AuthenticationConfig authenticationConfig,
                               UserDetailsService userDetailsService) {
        this.authenticationConfig = authenticationConfig;
        this.userDetailsService = userDetailsService;
    }

    void addAuthentication(HttpServletResponse res, String username) {
        final String secret = authenticationConfig.getSecret();
        final long expirationTime = authenticationConfig.getExpirationTime();

        String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
                .signWith(HS512, secret)
                .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
        try {
            res.getWriter().print("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        TokenData tokenData;
        if (token == null || (tokenData = parseDataFromToken(token)) == null) {
            return null;
        }

        final UserDetails details = userDetailsService.loadUserByUsername(tokenData.getSubject());
        return new ConnectedUser(details);
    }

    private TokenData parseDataFromToken(String token) {
        String claimRoles = "roles";
        try {
            Claims body = parseTokenBody(token);
            String subject = body.getSubject();
            List<String> roles = (List<String>) body.get(claimRoles);

            return TokenData.builder()
                    .subject(subject)
                    .expires(body.getExpiration().getTime())
                    .roles(roles)
                    .build();
        } catch (RuntimeException exception) {
            return null;
        }
    }

    private Claims parseTokenBody(String token) {
        return Jwts.parser()
                .setSigningKey(authenticationConfig.getSecret())
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody();
    }
}