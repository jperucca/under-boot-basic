package com.under.configuration.security.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.TemporalType.TIMESTAMP;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "secured_user")
public class SecuredUser {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 80)
    private String email;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private boolean enabled;

    @Column(name = "reset_date")
    @Temporal(TIMESTAMP)
    private Date lastPasswordResetDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_authority",
            joinColumns         = { @JoinColumn(name = "user_id", referencedColumnName = "id") },
            inverseJoinColumns  = { @JoinColumn(name = "authority_id", referencedColumnName = "id") })
    private List<Authority> authorities;
}
