package com.under.configuration.security.jwt;

import com.under.configuration.security.model.Authority;
import com.under.configuration.security.model.AuthorityName;
import com.under.configuration.security.model.SecuredUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtUserFactory {

    public JwtUser create(SecuredUser user) {
        return new JwtUser(
                user.getId(),
                user.getEmail(),
                user.getUsername(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getAuthorities()),
                user.isEnabled(),
                user.getLastPasswordResetDate()
        );
    }

    private List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream()
                .map(Authority::getRole)
                .map(AuthorityName::name)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}