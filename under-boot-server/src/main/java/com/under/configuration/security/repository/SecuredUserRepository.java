package com.under.configuration.security.repository;

import com.under.configuration.security.model.SecuredUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SecuredUserRepository extends JpaRepository<SecuredUser, Long> {

    SecuredUser findByUsername(String username);
}
