package com.under.configuration.security.jwt;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class TokenData {

    private String subject;
    private List<String> roles;
    private long expires;

}