package com.under.configuration.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.under.configuration.security.model.AccountCredential;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private final ObjectMapper objectMapper;
    private final TokenAuthenticationService tokenAuthenticationService;

    public JWTLoginFilter(String url,
                          AuthenticationManager authManager,
                          ObjectMapper objectMapper,
                          TokenAuthenticationService tokenAuthenticationService) {
        super(new AntPathRequestMatcher(url));
        Assert.notNull(objectMapper, "Object Mapper needed for credential");
        Assert.notNull(tokenAuthenticationService, "TokenAuthenticationService needed for authentication");

        setAuthenticationManager(authManager);
        this.objectMapper = objectMapper;
        this.tokenAuthenticationService = tokenAuthenticationService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException, IOException, ServletException {
        AccountCredential creds = objectMapper.readValue(req.getInputStream(), AccountCredential.class);

        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        creds.getUsername(),
                        creds.getPassword(),
                        Collections.emptyList()
                )
        );
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        tokenAuthenticationService.addAuthentication(res, auth.getName());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}