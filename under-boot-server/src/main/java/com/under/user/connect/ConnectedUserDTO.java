package com.under.user.connect;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConnectedUserDTO {

    private String username;
    private String email;
}