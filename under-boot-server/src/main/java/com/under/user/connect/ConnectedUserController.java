package com.under.user.connect;

import com.under.configuration.security.ConnectedUser;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.under.user.UserRoute.USER_ME_API;

@RestController
@RequestMapping(USER_ME_API)
public class ConnectedUserController {

    @GetMapping
    public ConnectedUserDTO getCurrentUser(@AuthenticationPrincipal ConnectedUser connectedUser) {
        return ConnectedUserDTO.builder()
                .username(connectedUser.getName())
                .email(connectedUser.getEmail())
                .build();
    }
}

