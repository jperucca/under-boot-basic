package com.under.user;

public class UserRoute {
    public static final String USER_API             = "/users";
    public static final String USER_ME_API          = USER_API + "/me";
    public static final String USER_SIGNUP_API      = USER_API  + "/signup";
}
