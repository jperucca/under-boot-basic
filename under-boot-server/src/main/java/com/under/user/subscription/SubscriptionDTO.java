package com.under.user.subscription;

import lombok.*;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

@Data
@Builder
@ToString(exclude = "password")
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionDTO {

    @NotNull
    private String username;

    @Email
    private String email;

    private String password;
}
