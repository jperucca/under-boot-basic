package com.under.user.subscription;

import com.under.configuration.security.model.Authority;
import com.under.configuration.security.model.SecuredUser;
import com.under.configuration.security.repository.AuthorityRepository;
import com.under.configuration.security.repository.SecuredUserRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.under.configuration.security.model.AuthorityName.ROLE_USER;
import static org.slf4j.LoggerFactory.getLogger;

@Service
public class SecuredUserService {

    private static final Logger logger = getLogger(SecuredUserService.class);

    private final SecuredUserRepository securedUserRepository;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SecuredUserService(SecuredUserRepository securedUserRepository,
                              AuthorityRepository authorityRepository,
                              PasswordEncoder passwordEncoder) {
        this.securedUserRepository = securedUserRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public void subscribe(SubscriptionDTO subscriptionDTO) {
        final List<Authority> authorities = authorityRepository.findByRole(ROLE_USER);
        final String encodedPassword = passwordEncoder.encode(subscriptionDTO.getPassword());

        final SecuredUser securedUser = SecuredUser.builder()
                .username(subscriptionDTO.getUsername())
                .email(subscriptionDTO.getEmail())
                .authorities(authorities)
                .enabled(true)
                .password(encodedPassword)
                .build();

        securedUserRepository.save(securedUser);

        logger.trace("[Subscription success] new account : {}", subscriptionDTO);
    }
}
