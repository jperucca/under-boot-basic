package com.under.user.subscription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.under.user.UserRoute.USER_SIGNUP_API;

@RestController
@RequestMapping(USER_SIGNUP_API)
public class SubscriptionController {

    private final SecuredUserService securedUserService;

    @Autowired
    public SubscriptionController(SecuredUserService securedUserService) {
        this.securedUserService = securedUserService;
    }

    @PostMapping
    public void subscribeOnPlatform(@RequestBody @Valid SubscriptionDTO subscriptionDTO,
                                    BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            // TODO
            System.out.println("Error on subscription " + subscriptionDTO);
        }

        System.out.println("Subscription with dto : " + subscriptionDTO);
        securedUserService.subscribe(subscriptionDTO);
    }
}
