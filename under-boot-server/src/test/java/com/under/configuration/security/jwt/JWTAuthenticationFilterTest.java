package com.under.configuration.security.jwt;

import com.under.configuration.security.ConnectedUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JWTAuthenticationFilterTest {

    @InjectMocks
    JWTAuthenticationFilter jwtAuthenticationFilter;

    @Mock TokenAuthenticationService tokenAuthenticationService;
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    @Mock FilterChain filterChain;
    @Mock ConnectedUser connectedUser;

    @Before
    public void init() {
        when(tokenAuthenticationService.getAuthentication(any())).thenReturn(connectedUser);
    }

    @Test
    public void should_add_authentication_to_security_context() throws IOException, ServletException {
        jwtAuthenticationFilter.doFilter(request, response, filterChain);

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertThat(authentication).isEqualTo(connectedUser);
        verify(tokenAuthenticationService).getAuthentication(any());
        verify(filterChain).doFilter(request, response);
    }
}