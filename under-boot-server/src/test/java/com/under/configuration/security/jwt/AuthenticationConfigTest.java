package com.under.configuration.security.jwt;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = {
            AuthenticationConfig.class,
            ConfigurationPropertiesAutoConfiguration.class
    },
    properties = {
            "auth.secret=ThisSecret",
            "auth.expiration-time=432000000"
    }
)
public class AuthenticationConfigTest {

    @Autowired
    AuthenticationConfig config;

    @Test
    public void should_be_well_formed() {
        assertThat(config.getSecret()).isEqualTo("ThisSecret");
        assertThat(config.getExpirationTime()).isEqualTo(432_000_000);
    }
}