package com.under.configuration.security.jwt;

import com.under.configuration.security.model.SecuredUser;
import com.under.configuration.security.repository.SecuredUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JwtUserDetailsServiceImplTest {

    @InjectMocks
    JwtUserDetailsServiceImpl jwtUserDetailsService;

    @Mock SecuredUserRepository securedUserRepository;
    @Mock JwtUserFactory jwtUserFactory;
    @Mock SecuredUser securedUser;

    @Test
    public void should_create_jwtUser_from_securedUser() {
        when(securedUserRepository.findByUsername(anyString())).thenReturn(securedUser);

        jwtUserDetailsService.loadUserByUsername("John");

        verify(jwtUserFactory).create(eq(securedUser));
    }

    @Test(expected = UsernameNotFoundException.class)
    public void should_throw_usernameNotFound_when_user_not_in_repo() {
        when(securedUserRepository.findByUsername(anyString())).thenReturn(null);

        jwtUserDetailsService.loadUserByUsername("unknown");
    }
}