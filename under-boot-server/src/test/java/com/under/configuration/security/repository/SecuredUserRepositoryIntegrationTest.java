package com.under.configuration.security.repository;

import com.under.configuration.security.model.SecuredUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SecuredUserRepositoryIntegrationTest {

    @Autowired
    SecuredUserRepository securedUserRepository;

    @Before
    public void setup_data() {
        SecuredUser securedUser = SecuredUser.builder().username("John").build();
        securedUserRepository.save(securedUser);
    }

    @Test
    public void should_find_by_username() {
        SecuredUser securedUser = securedUserRepository.findByUsername("John");

        assertThat(securedUser.getId()).isPositive();
        assertThat(securedUser.getUsername()).isEqualTo("John");
    }
}