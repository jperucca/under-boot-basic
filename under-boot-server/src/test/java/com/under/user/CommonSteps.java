package com.under.user;

import com.under.test.cucumber.CucumberStepDefinitions;
import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@CucumberStepDefinitions
public class CommonSteps {

    @Autowired
    StepContext stepContext;

    @Then("^(?:receive|I get|user gets) a (.*) responses$")
    public void receive_a_ok_response(final String statusCode) throws Throwable {
        stepContext.getResponse()
                .then()
                .statusCode(HttpStatus.valueOf(statusCode).value());
    }
}