package com.under.user;

import com.jayway.restassured.response.Response;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Scope("cucumber-glue")
@Component
public class StepContext {

    private Response response;

    @LocalServerPort
    private int serverPort;

    String getToken() {
        return response.then().extract().header("Authorization");
    }
}