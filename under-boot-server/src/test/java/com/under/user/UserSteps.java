package com.under.user;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.under.configuration.security.model.AccountCredential;
import com.under.test.cucumber.CucumberStepDefinitions;
import com.under.test.restassured.RestAssuredUtil;
import com.under.user.subscription.SubscriptionDTO;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.IsNull.notNullValue;

@CucumberStepDefinitions
public class UserSteps {

    @Autowired
    private StepContext stepContext;

    @Before
    public void initHttpClient() {
        RestAssuredUtil.init(stepContext);
    }

    @After
    public void teardown() {
        RestAssuredUtil.reset();
    }

    @Given("^one user exists$")
    public void one_user_exists() {
        // TODO : to add when resources/import.sql will be removed
    }

    @When("^user log in$")
    public void user_log_in() throws Throwable {
        AccountCredential accountCredential = new AccountCredential();
        accountCredential.setUsername("user");
        accountCredential.setPassword("password");

        Response response =
                given().log().all()
                    .contentType(ContentType.JSON)
                    .body(accountCredential)
                .when()
                    .post("/login");

        stepContext.setResponse(response);
    }

    @Then("^receive a jwt token in header$")
    public void receive_a_jwt_token_in_header() throws Throwable {
        stepContext.getResponse()
                .then()
                .log().all()
                .header("Authorization", allOf(
                        notNullValue(),
                        startsWith("Bearer"),
                        containsString("eyJhbGciOiJIUzUxMiJ9.")
                ));
    }

    @When("^user sign up$")
    public void user_sign_up() throws Throwable {
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.builder()
                .username("under")
                .email("under.64@gmail.com")
                .password("underpassword").build();

        Response response =
                given()
                    .contentType(ContentType.JSON)
                    .body(subscriptionDTO)
                    .log().all()
                .when()
                    .post("/users/signup");

        stepContext.setResponse(response);
    }

    @When("^user ask his info$")
    public void user_ask_his_info() throws Throwable {
        String token = stepContext.getToken();

        Response response =
                given()
                    .contentType(ContentType.JSON)
                    .header("Authorization", token)
                .when()
                    .get("/users/me");

        stepContext.setResponse(response);
    }

    @Then("^receive a complete dto$")
    public void receive_a_complete_dto() throws Throwable {
        stepContext.getResponse().then()
                .body("username", is("user"))
                .body("email", is("under@under.com"));
    }
}
