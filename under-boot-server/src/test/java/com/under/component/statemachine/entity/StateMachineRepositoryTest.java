package com.under.component.statemachine.entity;


import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StateMachineRepositoryTest {

    @Autowired
    StateMachineRepository stateMachineRepository;

    @Test
    public void should_persist_stateMachineEntity() {
        TreeSet<String> states = new TreeSet<>();
        String pending = "pending";
        states.addAll(asList(pending, "wip", "test"));
        Map<String, Pair<String, String>> transitions = new HashMap<>();
        transitions.put("to-wip", Pair.of(pending, "wip"));
        transitions.put("to-test", Pair.of("wip", "test"));

        StateMachineModelEntity stateMachineModelEntity = new StateMachineModelEntity();
        stateMachineModelEntity.setInitialState("pending");
        stateMachineModelEntity.setTerminalState("done");
        stateMachineModelEntity.setStates(states);
        stateMachineModelEntity.setTransitionsByEvent(new HashMap<>(transitions));

        StateMachineEntity stateMachineEntity = new StateMachineEntity();
        stateMachineEntity.setClientUuid("cli-123");
        stateMachineEntity.setStateMachineModelEntity(stateMachineModelEntity);

        StateMachineEntity entity = stateMachineRepository.save(stateMachineEntity);

        HashMap<String, Pair<String, String>> transitionsByEvent = entity.getStateMachineModelEntity().getTransitionsByEvent();

        assertThat(entity.getId()).isGreaterThan(0);
        assertThat(entity.getStateMachineModelEntity().getInitialState()).isEqualTo("pending");
        assertThat(entity.getStateMachineModelEntity().getStates()).containsAll(states);
        assertThat(transitionsByEvent)
                .containsKeys("to-wip", "to-test")
                .containsValues(Pair.of("pending", "wip"), Pair.of("wip", "test"));
    }
}