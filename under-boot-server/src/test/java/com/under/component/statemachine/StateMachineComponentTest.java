package com.under.component.statemachine;

import com.under.component.statemachine.domain.StateMachineDefinition;
import com.under.component.statemachine.domain.StateMachineModel;
import com.under.component.statemachine.exception.StateMachineCreationException;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;


@RunWith(SpringRunner.class)
@SpringBootTest
public class StateMachineComponentTest {

    @Autowired
    private StateMachineComponent stateMachineComponent;

    @Test
    public void should_convert_stateMachineModel_to_fully_functional_statemachine() {
        TreeSet<String> states = new TreeSet<>();
        String pending = "pending";
        states.addAll(asList(pending, "wip", "test"));

        Map<String, Pair<String, String>> transitions = new HashMap<>();
        transitions.put("to-wip", Pair.of(pending, "wip"));
        transitions.put("to-test", Pair.of("wip", "test"));

        StateMachineModel stateMachineModel = StateMachineModel.builder()
                .initialState(pending)
                .terminalState("done")
                .states(states)
                .transitionsByEvent(transitions)
                .build();

        StateMachineDefinition machineDefinition = null;
        try {
            machineDefinition = stateMachineComponent.convert(stateMachineModel);
        } catch (StateMachineCreationException e) {
            fail("StateMachine is not a valid one", e);
        }

        assertThat(machineDefinition.getInitialState()).isEqualTo(pending);
        StateMachine<String, String> stateMachine = machineDefinition.getStateMachine();
        assertThat(stateMachine).isNotNull();

        assertStates(states, stateMachine);
        assertTransitions(transitions, stateMachine);
        assertStateMachine(stateMachine);
    }

    private void assertStates(TreeSet<String> states, StateMachine<String, String> stateMachine) {
        List<String> stateList = stateMachine.getStates().stream().map(State::getId).collect(toList());
        assertThat(stateList).containsAll(states);
    }

    private void assertTransitions(Map<String, Pair<String, String>> transitionDefinition, StateMachine<String, String> stateMachine) {
        List<Pair<String, String>> stateMachineTransitions = stateMachine.getTransitions().stream().map(transition -> {
            String from = transition.getSource().getId();
            String to = transition.getTarget().getId();
            return Pair.of(from, to);
        }).collect(toList());

        stateMachineTransitions.forEach(transition -> System.out.println("from " + transition.getKey() + " to " + transition.getValue()));

        assertThat(stateMachineTransitions).containsAll(transitionDefinition.values());
    }

    private void assertStateMachine(StateMachine<String, String> stateMachine) {
        stateMachine.start();

        System.out.println("state : " + stateMachine.getState().getId());
        assertStateSwitch(StateSwitchContext.of("pending", "wip", "to-wip", true), stateMachine);

        System.out.println("state : " + stateMachine.getState().getId());
        assertStateSwitch(StateSwitchContext.of("wip", "test", "to-test", true), stateMachine);

        System.out.println("state : " + stateMachine.getState().getId());
        assertStateSwitch(StateSwitchContext.of("test", "done", "", false), stateMachine);

        stateMachine.stop();
    }

    private void assertStateSwitch(StateSwitchContext context, StateMachine<String, String> stateMachine) {
        String expectedStateTo = context.getExpectedResult() ? context.getTo() : context.getFrom();
        assertThat(stateMachine.getState().getId()).isEqualTo(context.getFrom());

        boolean result = stateMachine.sendEvent(context.getEvent());

        assertThat(result).isEqualTo(context.getExpectedResult());
        assertThat(stateMachine.getState().getId()).isEqualTo(expectedStateTo);
    }

    @Builder
    @Data
    private static class StateSwitchContext {
        private String from;
        private String to;
        private String event;
        private boolean expectedResult;

        public boolean getExpectedResult() {
            return expectedResult;
        }

        public static StateSwitchContext of(String from, String to, String event, boolean result) {
            return new StateSwitchContext(from, to, event, result);
        }
    }
}