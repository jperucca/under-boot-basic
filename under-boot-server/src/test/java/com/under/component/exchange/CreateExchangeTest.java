package com.under.component.exchange;

import com.under.component.client.domain.ClientContract;
import com.under.component.client.domain.ClientId;
import com.under.component.client.domain.SharingSystem;
import com.under.component.client.entity.ContractEntity;
import com.under.component.client.entity.SharingTypes;
import com.under.component.client.entity.ContractRepository;
import com.under.component.exchange.domain.*;
import com.under.component.exchange.entity.ExchangeEntity;
import com.under.component.exchange.entity.ExchangeRepository;
import com.under.component.exchange.request.CreateExchangeRequest;
import com.under.component.statemachine.domain.StateMachineDefinition;
import com.under.framework.BusinessException;
import com.under.test.statemachine.StateMachineTestUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.under.component.exchange.domain.TimeCategoryType.ONLY_ONCE;
import static com.under.component.exchange.domain.SharingType.GIVING;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CreateExchangeTest {

    @Autowired
    private ExchangeComponent exchangeComponent;

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ExchangeRepository exchangeRepository;

    @Autowired
    private ApplicationContext context;

    @Before
    public void initGivingContract() {
        ContractEntity contractEntity = new ContractEntity();
        contractEntity.setClientUuid(TestContext.clientUuid);
        contractEntity.setSharingTypes(SharingTypes.from(TestContext.giving));

        contractRepository.save(contractEntity);
    }

    @After
    public void clearData() {
        contractRepository.deleteAll();
    }

    @Test
    public void should_create_exchange_with_one_item() throws BusinessException {
        String owner = "usr-123";
        String receiver = "usr-987";
        TimeCategoryType onlyOnce = ONLY_ONCE;
        Items items = new Items(asList( createPhoneItem(owner) ));
        CreateExchangeRequest request = CreateExchangeRequest.builder()
                .items(items)
                .owner(owner)
                .receiver(receiver)
                .timeCategoryType(onlyOnce)
                .sharingSystem(createGivingSharingSystem())
                .contract(createClientContract())
                .build();

        Exchange exchange = exchangeComponent.createExchange(request);

        assertExchange(owner, receiver, onlyOnce, items, exchange);
        assertExchangeEntity(owner, receiver);
    }

    private ClientContract createClientContract(SharingType ineligibleSharingType) {
        return ClientContract.builder()
                .clientId(ClientId.from(TestContext.clientUuid))
                .sharingType(ineligibleSharingType)
                .build();
    }

    private ClientContract createClientContract() {
        return createClientContract(TestContext.giving);
    }

    private SharingSystem createGivingSharingSystem(){
        SharingSystem sharingSystem = null;
        try {
            StateMachine<String, String> stateMachine = StateMachineTestUtil.manualStateMachine(context.getAutowireCapableBeanFactory());
            StateMachineDefinition stateMachineDefinition = StateMachineDefinition.builder().stateMachine(stateMachine).build();

            sharingSystem = new SharingSystem(TestContext.giving, stateMachineDefinition);
        } catch (Exception e) {
            fail("Sharing system could not define state machine");
        }

        return sharingSystem;
    }

    private void assertExchange(String owner, String receiver, TimeCategoryType onlyOnce, Items items, Exchange exchange) {
        assertThat(exchange.getState()).isEqualTo("pending");
        Sharing sharing = exchange.getSharing();
        assertThat(sharing.getOwner()).isEqualTo(owner);
        assertThat(sharing.getReceiver()).isEqualTo(receiver);
        assertThat(sharing.getSharingType()).isEqualTo(TestContext.giving);
        assertThat(sharing.getItems()).isEqualTo(items);
        TimeCategory timeCategory = exchange.getTimeCategory();
        assertThat(timeCategory.getType()).isEqualTo(onlyOnce);
    }

    private void assertExchangeEntity(String owner, String receiver) {
        assertThat(exchangeRepository.count()).isEqualTo(1);
        Optional<ExchangeEntity> optionalExchangeEntity = exchangeRepository.findFirstByClientUuid(TestContext.clientUuid);
        assertThat(optionalExchangeEntity.isPresent()).isTrue();
        ExchangeEntity exchangeEntity = optionalExchangeEntity.get();
        assertThat(exchangeEntity.getId()).isGreaterThan(0);
        assertThat(exchangeEntity.getTimeCategoryType()).isEqualTo(ONLY_ONCE);
        assertThat(exchangeEntity.getOwner()).isEqualTo(owner);
        assertThat(exchangeEntity.getReceiver()).isEqualTo(receiver);
        assertThat(exchangeEntity.getState()).isEqualTo("pending");
        assertThat(exchangeEntity.getType()).isEqualTo(GIVING);
    }

    private Item createPhoneItem(String owner) {
        return Item.builder()
                    .uuid("phone-123")
                    .state(ItemState.from("pending"))
                    .name("phone")
                    .ownerUuid(owner)
                    .category(Category.from("hightech"))
                    .build();
    }

    static class TestContext {
        private static final String clientUuid = "society-1";
        private static final SharingType giving = GIVING;
    }
}