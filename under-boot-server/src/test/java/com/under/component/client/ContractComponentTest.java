package com.under.component.client;

import com.under.component.client.domain.ClientContract;
import com.under.component.client.domain.ClientId;
import com.under.component.client.domain.SharingSystem;
import com.under.component.client.entity.ContractEntity;
import com.under.component.client.entity.ContractRepository;
import com.under.component.client.entity.SharingTypes;
import com.under.component.client.exception.InEligibleException;
import com.under.component.client.exception.NoContractException;
import com.under.component.client.request.CreateClientContractRequest;
import com.under.component.exchange.CreateExchangeTest;
import com.under.component.exchange.domain.SharingType;
import com.under.component.exchange.request.CreateExchangeRequest;
import com.under.framework.BusinessException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.under.component.client.domain.ClientId.generate;
import static com.under.component.exchange.domain.SharingType.GIVING;
import static com.under.component.exchange.domain.SharingType.LOANING;
import static com.under.component.exchange.domain.SharingType.SELLING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractComponentTest {

    @Autowired
    ContractComponent contractComponent;

    @Autowired
    ContractRepository contractRepository;

    private SharingType giving = GIVING;

    @Test
    public void should_create_clientContract() {
        SharingType selling = SELLING;
        ClientContract clientContract = ClientContract.builder()
                .clientId(generate())
                .sharingType(selling)
                .build();
        CreateClientContractRequest request = CreateClientContractRequest.builder()
                .contract(clientContract)
                .sharingType(selling)
                .build();

        contractComponent.createClientContract(request);

        long contractCounter = contractRepository.count();
        assertThat(contractCounter).isEqualTo(2);
        Optional<ContractEntity> optionalContract = contractRepository.findByClientUuid(clientContract.getClientId().getUuid());
        assertThat(optionalContract.isPresent()).isTrue();
        assertThat(optionalContract.get().getSharingTypes().getValues()).contains(selling);
    }

    @Test
    public void should_validate_contract() {
        SharingSystem sharingSystem = new SharingSystem(giving, null);
        ClientContract clientContract = ClientContract.builder()
                .clientId( ClientId.from("usr-123") )
                .sharingType(giving)
                .build();
        CreateExchangeRequest request = CreateExchangeRequest.builder()
                .sharingSystem(sharingSystem)
                .contract(clientContract)
                .build();

        try {
            contractComponent.checkValidity(request);
        } catch (BusinessException e) {
            fail("Contract should have been valid");
        }
    }

    @Test(expected = InEligibleException.class)
    public void should_throw_ineligibleException_when_contract_ineligible() throws BusinessException {
        final SharingType loaning = LOANING;
        SharingSystem sharingSystem = new SharingSystem(loaning, null);
        ClientContract clientContract = ClientContract.builder()
                .clientId( ClientId.from("usr-123") )
                .sharingType(loaning)
                .build();
        CreateExchangeRequest request = CreateExchangeRequest.builder()
                .sharingSystem(sharingSystem)
                .contract(clientContract)
                .build();

        contractComponent.checkValidity(request);
    }

    @Test(expected = NoContractException.class)
    public void should_throw_ineligibleException_when_contract_not_found() throws BusinessException {
        contractRepository.deleteAll();

        SharingSystem sharingSystem = new SharingSystem(giving, null);
        ClientContract clientContract = ClientContract.builder()
                .clientId( ClientId.from("usr-123") )
                .sharingType(giving)
                .build();
        CreateExchangeRequest request = CreateExchangeRequest.builder()
                .sharingSystem(sharingSystem)
                .contract(clientContract)
                .build();

        contractComponent.checkValidity(request);
    }

    @Before
    public void initGivingContract() {
        ContractEntity contractEntity = new ContractEntity();
        contractEntity.setClientUuid("usr-123");
        contractEntity.setSharingTypes(SharingTypes.from(giving));

        contractRepository.save(contractEntity);
    }

    @After
    public void clearData() {
        contractRepository.deleteAll();
    }
}