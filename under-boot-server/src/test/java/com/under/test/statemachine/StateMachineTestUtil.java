package com.under.test.statemachine;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;

import java.util.HashSet;
import java.util.List;

import static java.util.Arrays.asList;

public final class StateMachineTestUtil {

    private static final List<String> states = asList("pending", "wip", "test");
    private static final List<String> events = asList("to-wip", "to-test");

    public static StateMachine<String, String> manualStateMachine(BeanFactory beanFactory) throws Exception {
        return buildStateMachine(beanFactory, false);
    }

    public static StateMachine<String, String> autoStartupStateMachine(BeanFactory beanFactory) throws Exception {
        return buildStateMachine(beanFactory, true);
    }

    private static StateMachine<String, String> buildStateMachine(BeanFactory beanFactory, boolean autoStartup) throws Exception {
        StateMachineBuilder.Builder<String, String> builder = StateMachineBuilder.builder();

        builder.configureStates().withStates()
                .initial(states.get(0))
                .end("done")
                .states(new HashSet<>(states));

        builder.configureTransitions().withExternal()
                .source(states.get(0)).target(states.get(1)).event(events.get(0)).and().withExternal()
                .source(states.get(1)).target(states.get(2)).event(events.get(1)).and().withExternal();

        builder.configureConfiguration().withConfiguration()
                .beanFactory(beanFactory)
                .autoStartup(autoStartup);

        return builder.build();
    }
}
