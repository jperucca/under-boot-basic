package com.under.test.restassured;

import com.jayway.restassured.RestAssured;
import com.under.user.StepContext;

public class RestAssuredUtil {

    public static void init(StepContext stepContext) {
        RestAssured.port = stepContext.getServerPort();
    }

    public static void reset() {
        RestAssured.reset();
    }
}
